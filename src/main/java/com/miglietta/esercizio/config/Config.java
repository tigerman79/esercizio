package com.miglietta.esercizio.config;

import com.miglietta.esercizio.model.Plane;
import com.miglietta.esercizio.model.Point;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
    public Plane getPlane(){
        return new Plane();
    }
}
