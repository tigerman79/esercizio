package com.miglietta.esercizio.controller;

import com.miglietta.esercizio.dto.PointDto;
import com.miglietta.esercizio.service.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class EsercizioController {

    @Autowired
    PointService service;

    @PostMapping("/point")
    public ResponseEntity addPoint(@RequestBody PointDto dto) {
        service.addPointToSpace(dto);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/space")
    public ResponseEntity getAllPoints() {
        return retrieveResponseByCollection(service.getAllPoints());
    }

    @GetMapping("/lines/{n}")
    public ResponseEntity getLinesWithCollinearPoints(@PathVariable("n") int n) {

        if(n < 2)
            return ResponseEntity.badRequest().body("N deve essere maggiore di 1");

        return retrieveResponseByCollection(service.getLines(n));
    }

    @DeleteMapping("/space")
    public ResponseEntity reset() {
        service.resetSpace();
        return ResponseEntity.ok().build();
    }

    private ResponseEntity retrieveResponseByCollection(Collection c) {
        if(c.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(c);
    }
}
