package com.miglietta.esercizio.exception;

public class InvalidLineException extends Exception {

    public InvalidLineException(final String message) {
        super(message);
    }
}
