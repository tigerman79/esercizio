package com.miglietta.esercizio.model;

import com.miglietta.esercizio.exception.InvalidLineException;

import java.util.*;
import java.util.stream.Collectors;

public class Plane {

    private Set<Point> points;
    private Map<Integer, List<Line>> lines;

    public Plane(){
        init();
    }


    public void addPoint(final Point point) {
        if(!points.contains(point)) {
            recomputeLines(point);
            this.points.add(point);
        }
    }

    public Set<Point> getPoints() {
        return points;
    }

    public List<Line> getLines(int n) {
        return this.lines.entrySet()
                .stream()
                .filter(v -> v.getKey() >= n)
                .map(Map.Entry::getValue)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private void recomputeLines(final Point point) {
        Map<Integer, List<Line>> lines = new HashMap<>();

        this.lines.values()
                .stream()
                .flatMap(Collection::stream)
                .forEach(line -> {
                    if(isPointToLine(point, line)) {
                        line.addPoint(point);
                    }
                    List<Line> lineList = lines.get(line.getAllPoints().size());
                    if(lineList == null) {
                        lineList = new ArrayList<>();
                    }
                    lineList.add(line);
                    lines.put(line.getAllPoints().size(), lineList);
                });

        this.lines = lines;
        drawNewLines(point);
    }

    private void drawNewLines(final Point point)  {
        final List<Line> twoPointsLine;
        if(this.lines.get(2) != null)
            twoPointsLine = this.lines.get(2);
        else
            twoPointsLine = new ArrayList<>();


        this.points
                .forEach(p -> {
                    if(! alreadyInLine(point, p)) {
                        try {
                            twoPointsLine.add(Line.of(point, p));
                        } catch (InvalidLineException e) {
                            e.printStackTrace();
                        }
                    }

                });

        if(!twoPointsLine.isEmpty())
            this.lines.put(2, twoPointsLine);

    }

    private boolean alreadyInLine(Point p1, Point p2) {
        List<Point> points = new ArrayList<>();
        points.add(p1);
        points.add(p2);

        return this.lines.values()
                .stream()
                .flatMap(Collection::stream)
                .anyMatch(line -> line.getAllPoints().containsAll(points));
    }

    public boolean isPointToLine(final Point p, final Line line) {
        final List<Point> linePoints = line.getAllPoints();
        final Point p1 = linePoints.get(0);
        final Point p2 = linePoints.get(1);

        double result = (p2.getY() - p1.getY()) * p.getX();
        result += (p1.getX() - p2.getX()) * p.getY();
        result += (p2.getX() * p1.getY() - p1.getX() * p2.getY());

        return result == 0d;
    }

    public void clear() { 
        init();
    }

    private void init() {
        points=new HashSet<>();
        this.lines = new HashMap<>();
    }
}
