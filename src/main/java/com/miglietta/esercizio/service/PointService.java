package com.miglietta.esercizio.service;

import com.miglietta.esercizio.dto.PointDto;
import com.miglietta.esercizio.model.Plane;
import com.miglietta.esercizio.model.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class PointService {

    @Autowired
    private Plane plane;

    private Function<Point, PointDto> pointDtoMapper = p -> new PointDto(p.getX(), p.getY());

    public void addPointToSpace(final PointDto dto) {
        plane.addPoint(Point.of(dto.getX(), dto.getY()));
    }

    public Collection<PointDto> getAllPoints() {

        return this.plane.getPoints()
                .stream()
                .map(pointDtoMapper)
                .collect(Collectors.toSet());
    }

    public List<List<PointDto>> getLines(int n) {

        return this.plane.getLines(n)
                .stream()
                .map(l -> l.getAllPoints()
                        .stream()
                        .map(pointDtoMapper)
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
    }

    public void resetSpace() {
        plane.clear();
    }
}
